let numberOne = +prompt('Enter first number', '99');
while (isNaN(numberOne)) {
    numberOne = +prompt('Enter your first number again ', numberOne)
}

let numberSecond = +prompt('Enter second number', '20');
while (isNaN(numberSecond)) {
    numberSecond = +prompt('Enter your second number again ', numberSecond)
}
let numbersOperation = prompt('Enter operation','+');


function calculate(a, b, operation) {

    switch (operation) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }}

console.log(calculate(numberOne, numberSecond, numbersOperation));
